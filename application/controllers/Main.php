<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('offers_model');
		$this->load->database();
	}

	public function index() {
		$this->load->view('main');
	}

	public function adscendMediaPostback() {

		$adscendIp = "54.204.57.82"; // ip for adsc

		//if($_SERVER['REMOTE_ADDR'] != $adscendIp) {
		//	die("Access Denied!");
		//}

		$offer = array(
			'userID' => $this->input->get('userID'),
			'transactionID' => $this->input->get('transactionID'),
			'offerID' => $this->input->get('offerID'),
			'offerName' => $this->input->get('offerName'),
			'payment' => $this->input->get('payment'),
			'commision' => $this->input->get('commision'),
			'status' => $this->input->get('status'),
			'ip' => $this->input->get('ip'),
			'date' => date("Y-m-d H:i:s")
		);

		$this->offers_model->add_offer($offer);
		if($offer['status'] == "1") {
		}
		else {
			die("Revoked Lead!");
		}
	}

}