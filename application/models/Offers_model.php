<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Offers_model extends CI_Model {

	public $userID;
	public $transactionID;
	public $offerID;
	public $offerName;
	public $payment;
	public $commision;
	public $status;
	public $ip;

	public function add_offer($offer) {
		$this->db->insert('offers', $offer);
	}

}


?>